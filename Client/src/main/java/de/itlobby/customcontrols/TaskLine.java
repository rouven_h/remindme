package de.itlobby.customcontrols;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.Task;
import de.itlobby.models.User;
import de.itlobby.services.TaskService;
import de.itlobby.util.ControlUtil;
import de.itlobby.util.StringUtil;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class TaskLine extends HBox
{
    private Task task;

    public TaskLine(Task task, User user)
    {
        TaskService taskService = ServiceLocator.getServiceInstance(TaskService.class);

        setSpacing(10);
        setAlignment(Pos.CENTER_LEFT);
        setPadding(new Insets(0, 5, 0, 5));

        TextFlow txtText = new TextFlow();
        txtText.getChildren().add(new Text(task.getText()));
        txtText.setMinHeight(30);
        HBox.setHgrow(txtText, Priority.ALWAYS);

        TextFlow txtEndDate = new TextFlow();
        txtEndDate.getChildren().add(new Text(StringUtil.dateToString(task.getEndDate())));

        CheckBox chkIsDone = new CheckBox();
        chkIsDone.setOnAction(event -> taskService.onMarkAsDone(chkIsDone.isSelected(), txtText, this, user, task));
        chkIsDone.setSelected(task.getIsDone());
        chkIsDone.setTooltip(new Tooltip("Ist die Aufgabe erledigt?"));

        if (task.getIsDone())
        {
            ControlUtil.addCSSClass(this, "task_row_done");
            txtText.setDisable(true);
        }
        else
        {
            ControlUtil.setOnlyCSSClass(this, "task_row");
            txtText.setDisable(false);
        }

        getChildren().addAll(txtText, txtEndDate, chkIsDone);

        String css = "css/MainStyle.css";
        getStylesheets().add(css);
        getStyleClass().add("task_row");
        txtText.getStylesheets().add(css);
        txtText.getStyleClass().add("task_row_textfield");

        this.task = task;
    }

    public Task getTask()
    {
        return task;
    }
}
