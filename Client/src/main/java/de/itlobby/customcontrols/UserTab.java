package de.itlobby.customcontrols;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.User;
import de.itlobby.services.TaskService;
import de.itlobby.util.ControlUtil;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class UserTab extends Tab
{
    private final VBox rootLayout;
    private User tabUser;
    private ListView currentTaskListView;

    public UserTab(User user)
    {
        super(createTitle(user));

        tabUser = user;

        rootLayout = new VBox(10);
        rootLayout.getStylesheets().add("css/MainStyle.css");
        rootLayout.getStyleClass().add("bg_transparent");


        //Button erstellen
        HBox hBox = new HBox(10);

        TaskService taskService = ServiceLocator.getServiceInstance(TaskService.class);

        Button addButton = new Button("Hinzufügen");
        addButton.setOnAction(event -> taskService.onAddTask());
        ControlUtil.addCSSClass(addButton, "color_button");

        Button editButton = new Button("Bearbeiten");
        editButton.setOnAction(event -> taskService.onEditTask());
        ControlUtil.addCSSClass(editButton, "color_button");

        Button removeButton = new Button("Löschen");
        removeButton.setOnAction(event -> taskService.onRemoveTask());
        ControlUtil.addCSSClass(removeButton, "color_button");

        hBox.getChildren().addAll(addButton, editButton, removeButton);

        VBox.setMargin(hBox, new Insets(5));

        ListView userTaskListView = ControlUtil.createUserTaskListView(user);
        updateButtonState(editButton, removeButton, userTaskListView);

        userTaskListView.getSelectionModel().selectedItemProperty().addListener(
            (observable, oldValue, newValue) -> updateButtonState(editButton, removeButton, userTaskListView)
        );

        this.currentTaskListView = userTaskListView;

        rootLayout.getChildren().addAll(
            hBox,
            userTaskListView
        );

        setContent(rootLayout);
    }

    private static String createTitle(User user)
    {
        return user.getName() + " (" + user.getTaskList().size() + ")";
    }

    private void updateButtonState(Button editButton, Button removeButton, ListView userTaskListView)
    {
        editButton.setDisable(userTaskListView.getSelectionModel().getSelectedItem() == null);
        removeButton.setDisable(userTaskListView.getSelectionModel().getSelectedItem() == null);
    }

    public void updateTaskListView(User modifiedUser)
    {
        tabUser = modifiedUser;

        if (rootLayout.getChildren().size() > 1)
        {
            rootLayout.getChildren().remove(1);
        }

        ListView userTaskListView = ControlUtil.createUserTaskListView(tabUser);

        this.currentTaskListView = userTaskListView;

        rootLayout.getChildren().add(userTaskListView);

        setText(createTitle(tabUser));
    }

    public User getTabUser()
    {
        return tabUser;
    }

    public ListView getCurrentTaskListView()
    {
        return currentTaskListView;
    }
}
