package de.itlobby.framework;

import de.itlobby.models.ViewController;
import de.itlobby.util.ExceptionUtil;
import de.itlobby.util.SystemUtil;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

public class FXMLFactory
{
    private HashMap<Views, Scene> viewList;
    private HashMap<Views, ViewController> viewControllerList;

    public FXMLFactory()
    {
        viewList = new HashMap<>();
        viewControllerList = new HashMap<>();
    }

    public Scene getView(Views viewToLoad)
    {
        Scene scene = viewList.get(viewToLoad);

        if (scene == null)
        {
            scene = createView(viewToLoad);
        }

        return scene;
    }

    private Scene createView(Views viewToLoad)
    {
        URL resource = SystemUtil.getResourceURL(viewToLoad.getPath());

        try
        {
            FXMLLoader loader = new FXMLLoader(resource);
            Parent load = loader.load();
            Scene scene = new Scene(load);
            ViewController controller = loader.getController();

            viewList.put(viewToLoad, scene);
            viewControllerList.put(viewToLoad, controller);

            return scene;
        }
        catch (IOException e)
        {
            ExceptionUtil.logException(e);
        }

        return null;
    }

    public void initialize()
    {
        for (Views view : Views.values())
        {
            createView(view);
        }
    }

    public ViewController getViewController(Views view)
    {
        return viewControllerList.get(view);
    }
}
