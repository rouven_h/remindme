package de.itlobby.framework;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class ViewManager
{
    private static ViewManager instance;
    private Stage primaryStage;
    private FXMLFactory fxmlFactory;

    private ViewManager()
    {
    }

    public static ViewManager getInstance()
    {
        if (instance == null)
        {
            instance = new ViewManager();
        }

        return instance;
    }

    public void initialize()
    {
        fxmlFactory = new FXMLFactory();
        fxmlFactory.initialize();

        activateView(Views.LoginView);

        primaryStage.show();
    }

    public void activateView(Views viewToLoad)
    {
        if (primaryStage != null)
        {
            Scene scene = fxmlFactory.getView(viewToLoad);
            primaryStage.setScene(scene);
            primaryStage.setTitle(viewToLoad.getTitle());
        }
    }

    public <T> T getViewController(Views view)
    {
        return (T) (view.getClazz()).cast(fxmlFactory.getViewController(view));
    }

    public Stage getPrimaryStage()
    {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
    }
}
