package de.itlobby.framework;

import de.itlobby.models.ViewController;
import de.itlobby.viewcontroller.LoginViewController;
import de.itlobby.viewcontroller.MainViewController;

public enum Views
{
    MainView("views/MainView.fxml", "RemindMe", MainViewController.class),
    LoginView("views/LoginView.fxml", "RemindMe - Login", LoginViewController.class);

    private final String path;
    private final Class<ViewController> clazz;
    private String title;

    Views(String path, String title, Class<?> clazz)
    {
        this.path = path;
        this.title = title;
        this.clazz = (Class<ViewController>) clazz;
    }

    public String getPath()
    {
        return path;
    }

    public String getTitle()
    {
        return title;
    }

    public Class<ViewController> getClazz()
    {
        return clazz;
    }
}
