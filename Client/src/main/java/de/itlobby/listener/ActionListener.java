package de.itlobby.listener;

public interface ActionListener
{
    void onAction();
}
