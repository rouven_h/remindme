package de.itlobby.models;

import java.util.Date;

public class Task
{
    Long id;
    String text;
    Boolean isDone;
    Date endDate;

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Boolean getIsDone()
    {
        return isDone;
    }

    public void setIsDone(Boolean isDone)
    {
        this.isDone = isDone;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Task task = (Task) o;

        if (endDate != null ? !endDate.equals(task.endDate) : task.endDate != null)
        {
            return false;
        }
        if (isDone != null ? !isDone.equals(task.isDone) : task.isDone != null)
        {
            return false;
        }
        if (text != null ? !text.equals(task.text) : task.text != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (isDone != null ? isDone.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}

