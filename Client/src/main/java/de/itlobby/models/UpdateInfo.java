package de.itlobby.models;

import java.io.Serializable;

public class UpdateInfo implements Serializable
{
    private boolean needUpdate;
    private UpdateType updateType;

    public UpdateInfo()
    {
    }

    public boolean isNeedUpdate()
    {
        return needUpdate;
    }

    public void setNeedUpdate(boolean needUpdate)
    {
        this.needUpdate = needUpdate;
    }

    public UpdateType getUpdateType()
    {
        return updateType;
    }

    public void setUpdateType(UpdateType updateType)
    {
        this.updateType = updateType;
    }
}
