package de.itlobby.models;

import java.io.Serializable;

public enum UpdateType implements Serializable
{
    MISC,
    ADD,
    EDIT,
    REMOVE
}
