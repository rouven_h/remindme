package de.itlobby.models;

public enum WsCallType
{
    USER("user"),
    MACHINE("machine"),
    TASK("task");

    private String path;

    WsCallType(String path)
    {
        this.path = path;
    }

    public String getPath()
    {
        return path;
    }
}
