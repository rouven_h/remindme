package de.itlobby.notification;

import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Demo extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        String message = "Es haben sich folgende Benutzer neu angemeldet:\n";
        message += "Rouven\n";
        message += "Peter\n";

        TrayNotification.show(
            "Neuer Benutzer",
            message.trim(),
            new Image("file:\\D:\\JavaProjekte\\RemindMe\\Client\\src\\main\\resources\\icon\\icon_32x32.png"),
            primaryStage
        );
    }
}
