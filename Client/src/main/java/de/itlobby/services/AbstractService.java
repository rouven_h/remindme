package de.itlobby.services;

import de.itlobby.framework.ViewManager;
import de.itlobby.framework.Views;
import de.itlobby.viewcontroller.LoginViewController;
import de.itlobby.viewcontroller.MainViewController;

public abstract class AbstractService
{
    LoginViewController loginViewController;
    MainViewController mainViewController;

    public AbstractService()
    {
        loginViewController = ViewManager.getInstance().getViewController(Views.LoginView);
        mainViewController = ViewManager.getInstance().getViewController(Views.MainView);
    }
}
