package de.itlobby.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.models.Task;
import de.itlobby.models.UpdateInfo;
import de.itlobby.models.User;
import de.itlobby.models.WsCallType;
import de.itlobby.settings.Settings;
import de.itlobby.util.ExceptionUtil;
import de.itlobby.util.MapUtil;
import javafx.application.Platform;

import javax.ws.rs.core.MediaType;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BackendService
{
    private static BackendService instance;
    private final String servername;
    private final String port;
    private Timer updateCheckTimer;
    private boolean isOnline;

    private BackendService()
    {
        Settings settings = Settings.getInstance();

        servername = settings.getConfig().getServerName();
        port = settings.getConfig().getServerPort();
    }

    public static BackendService getInstance()
    {
        if (instance == null)
        {
            instance = new BackendService();
        }

        return instance;
    }

    private String getBaseUrl(WsCallType type)
    {
        String baseUrl = "http://" + servername + ":" + port + "/";

        return baseUrl + type.getPath() + "/";
    }

    public boolean checkWebService()
    {
        try
        {
            new URL(getBaseUrl(WsCallType.USER) + "ping").openStream();
            isOnline = true;
        }
        catch (Exception e)
        {
            isOnline = false;
        }

        return isOnline;
    }

    public List<User> getAllUser()
    {
        User[] wsResult = restGET(User[].class, "", WsCallType.USER);
        return new ArrayList<>(Arrays.asList(wsResult));
    }

    private <T> T restPOST(T obj, String method, WsCallType type)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl(type) + method);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, MapUtil.toJSON(obj));
        return MapUtil.toObject(response.getEntity(String.class), (Class<T>) obj.getClass());
    }

    private <T> T restPOSTArray(String objArray, String method, WsCallType type, Class<T> targetClass)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl(type) + method);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).post(ClientResponse.class, MapUtil.toJSON(objArray));
        return MapUtil.toObject(response.getEntity(String.class), targetClass);
    }

    private boolean restDELETE(String method, WsCallType type)
    {
        Client client = Client.create(new DefaultClientConfig());
        WebResource webResource = client.resource(getBaseUrl(type) + method);
        ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
        return MapUtil.toObject(response.getEntity(String.class), Boolean.class);
    }

    public User getUser(String username)
    {
        return restGET(User.class, username, WsCallType.USER);
    }

    public Boolean deleteUser(String username)
    {
        return restDELETE(username, WsCallType.USER);
    }

    public Boolean deleteTask(Task task)
    {
        boolean delete = restDELETE(task.getId().toString(), WsCallType.TASK);
        registerMachine();
        return delete;
    }

    public Task updateTask(Task task)
    {
        Task updatedTask = restPOST(task, "update", WsCallType.TASK);
        registerMachine();
        return updatedTask;
    }

    public User createTask(Long id, Task newTask)
    {
        String jsonArrayParam = MapUtil.createJsonParamArray(id, newTask);

        User updatedUser = restPOSTArray(jsonArrayParam, "create", WsCallType.TASK, User.class);
        registerMachine();
        return updatedUser;
    }

    private <T> T restGET(Class<T> targetClass, String wsMethodName, WsCallType type)
    {
        String jsonContent = MapUtil.readJsonContentFromUrl(getBaseUrl(type), wsMethodName);
        return MapUtil.toObject(jsonContent, targetClass);
    }

    public User updateUser(User tabUser)
    {
        User user = restPOST(tabUser, "update", WsCallType.USER);
        registerMachine();
        return user;
    }

    private String getMachineName()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void registerMachine()
    {
        restPOST(getMachineName(), "registerMachine", WsCallType.MACHINE);
    }

    public void unregisterMachine()
    {
        String machineName = getMachineName();
        restDELETE(machineName, WsCallType.MACHINE);
    }

    public void createUpdateTimer()
    {
        updateCheckTimer = new Timer();
        updateCheckTimer.schedule(
            new TimerTask()
            {
                @Override
                public void run()
                {
                    timerTick();
                }
            }, 0, 7500
        );
    }

    private void timerTick()
    {
        boolean needUpdate = false;
        UpdateInfo updateInfo = null;
        try
        {
            if (isOnline)
            {
                updateInfo = restGET(UpdateInfo.class, getMachineName(), WsCallType.MACHINE);
                needUpdate = updateInfo != null && updateInfo.isNeedUpdate();
            }
            else
            {
                unload();
            }
        }
        catch (Exception e)
        {
            ExceptionUtil.logException(e);
        }

        if (needUpdate)
        {
            //System.out.println("Need Update");

            final UpdateInfo finalUpdateInfo = updateInfo;
            Platform.runLater(() -> ServiceLocator.getServiceInstance(TaskService.class).executeUserUpdate(finalUpdateInfo));
        }
        else
        {
            //System.out.println("Need NO Update");
        }
    }

    public void unload()
    {
        if (checkWebService())
        {
            unregisterMachine();
        }

        disableUpdateTimer();
    }

    public void disableUpdateTimer()
    {
        if (updateCheckTimer != null)
        {
            updateCheckTimer.cancel();
            updateCheckTimer.purge();
        }
    }
}
