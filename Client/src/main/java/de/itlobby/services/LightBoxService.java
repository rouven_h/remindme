package de.itlobby.services;

import de.itlobby.listener.ActionListener;
import de.itlobby.util.ControlUtil;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class LightBoxService extends AbstractService
{
    public void showDialog(String title, Parent content, ActionListener cancelListener, ActionListener okListener)
    {
        HBox buttonLayout = new HBox(10);
        buttonLayout.setAlignment(Pos.CENTER_RIGHT);

        Button cancelButton = new Button();
        cancelButton.setText("Abbrechen");
        cancelButton.setOnAction(event -> cancelListener.onAction());
        cancelButton.setCancelButton(true);
        ControlUtil.addCSSClass(cancelButton, "color_button");

        Button okButton = new Button();
        okButton.setText("Ok");
        okButton.setOnAction(event -> okListener.onAction());
        okButton.setDefaultButton(true);
        ControlUtil.addCSSClass(okButton, "color_button");

        buttonLayout.getChildren().add(cancelButton);
        buttonLayout.getChildren().add(okButton);

        VBox rootBox = new VBox(10);
        rootBox.getChildren().add(content);
        rootBox.getChildren().add(buttonLayout);

        VBox.setVgrow(content, Priority.ALWAYS);
        VBox.setVgrow(buttonLayout, Priority.NEVER);

        TitledPane titledPane = new TitledPane(title, rootBox);
        titledPane.setVisible(true);
        titledPane.setMaxHeight(250);
        titledPane.setMaxWidth(350);
        titledPane.setCollapsible(false);

        mainViewController.lbBorderPane.setVisible(true);
        mainViewController.lbBorderPane.setCenter(titledPane);
    }

    public void hideDialog()
    {
        mainViewController.lbBorderPane.setVisible(false);
        mainViewController.lbBorderPane.setCenter(null);
    }
}
