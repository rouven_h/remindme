package de.itlobby.services;

import de.itlobby.customcontrols.UserTab;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.framework.ViewManager;
import de.itlobby.framework.Views;
import de.itlobby.models.User;
import de.itlobby.settings.Settings;
import javafx.application.Platform;
import javafx.scene.Cursor;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.util.List;
import java.util.stream.Collectors;

public class MainService extends AbstractService
{
    public MainService()
    {
        initialize();
    }

    private void initialize()
    {
        loginViewController.btnLogin.setOnAction((event) -> startLogin());
        loginViewController.btnExit.setOnAction(event -> beforeShutdown());
        loginViewController.imgBackendStatus.setOnMouseClicked(event -> checkServerConnection());

        mainViewController.btnCloseApplication.setOnAction(event -> beforeShutdown());
        mainViewController.btnLogout.setOnAction(event -> logout());
        mainViewController.btnDeleteUser.setOnAction(event -> onDeleteSelectedUser());

        checkServerConnection();
    }

    private void onDeleteSelectedUser()
    {
        UserTab selectedTab = (UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem();
        User tabUser = selectedTab.getTabUser();

        LightBoxService dialog = ServiceLocator.getServiceInstance(LightBoxService.class);
        TextFlow msgText = new TextFlow(
            new Text("Möchten sie den Benutzer " + tabUser.getName() + " wirklich löschen ?\n" +
                         "Es werden alle Aufgaben unwiderruflich gelöscht."
            )
        );
        dialog.showDialog("Achtung!", msgText, dialog::hideDialog, () -> deleteSelectedUser(selectedTab, dialog));
    }

    private void deleteSelectedUser(UserTab selectedTab, LightBoxService dialog)
    {
        dialog.hideDialog();
        User tabUser = selectedTab.getTabUser();
        Boolean delOk = BackendService.getInstance().deleteUser(tabUser.getName());

        if (delOk)
        {
            if (tabUser.equals(Settings.getInstance().getCurrentUser()))
            {
                logout();
            }
            else
            {
                mainViewController.tabUserTasks.getTabs().remove(selectedTab);
            }
        }
        else
        {
            LightBoxService warnDialog = ServiceLocator.getServiceInstance(LightBoxService.class);
            TextFlow msgText = new TextFlow(
                new Text("Das Löschen des Benutzers " + tabUser.getName() + " war nicht erfolgreich."
                )
            );
            warnDialog.showDialog("Achtung!", msgText, warnDialog::hideDialog, null);
        }
    }

    private void checkServerConnection()
    {
        new Thread(() -> {
            boolean isOnline = BackendService.getInstance().checkWebService();

            Platform.runLater(
                () -> {
                    {
                        loginViewController.btnLogin.setDisable(!isOnline);

                        String path = isOnline ? "images/online.png" : "images/offline.png";
                        loginViewController.imgBackendStatus.setImage(new Image(path));
                        loginViewController.imgBackendStatus.setCursor(isOnline ? Cursor.DEFAULT : Cursor.HAND);

                        if (isOnline)
                        {
                            BackendService.getInstance().registerMachine();
                        }
                    }
                }
            );
        }
        ).start();
    }

    private void logout()
    {
        ViewManager.getInstance().activateView(Views.LoginView);
        BackendService.getInstance().disableUpdateTimer();
    }

    public void beforeShutdown()
    {
        BackendService.getInstance().unload();
        Platform.exit();
    }

    private void startLogin()
    {
        String userText = loginViewController.txtUsername.getText();

        String errorMsg = checkLogin(userText.trim());

        if (errorMsg.length() <= 0)
        {
            hideErrorMsg();

            ViewManager.getInstance().activateView(Views.MainView);

            User currentUser = BackendService.getInstance().getUser(userText);
            Settings.getInstance().setCurrentUser(currentUser);

            refreshTabView();

            BackendService.getInstance().createUpdateTimer();
        }
        else
        {
            showErrorMsg(errorMsg);
        }
    }

    public void refreshTabView()
    {
        User currentUser = Settings.getInstance().getCurrentUser();

        List<User> userList = BackendService.getInstance().getAllUser();

        for (User user : userList)
        {
            user.setTaskList(user.getTaskList().stream().sorted((o1, o2) -> (o1.getEndDate().compareTo(o2.getEndDate())) * -1).collect(Collectors.toList()));
        }

        List<UserTab> tabs = userList.stream().map(UserTab::new).collect(Collectors.toList());

        UserTab currentUserTab = tabs.stream().filter(x -> x.getText().startsWith(currentUser.getName())).collect(Collectors.toList()).get(0);

        mainViewController.tabUserTasks.getTabs().clear();
        mainViewController.tabUserTasks.getTabs().addAll(tabs);

        mainViewController.tabUserTasks.getSelectionModel().select(currentUserTab);
    }

    private String checkLogin(String username)
    {
        String errorMsg = "";

        if (username == null || username.equals(""))
        {
            errorMsg += "Bitte geben sie einen Benutzernamen ein.";
        }
        else if (username.contains(" "))
        {
            errorMsg += "Der Benutzername darf keine Leerzeichen enthalten.";
        }

        return errorMsg.trim();
    }

    private void showErrorMsg(String errorMsg)
    {
        Label txtError = loginViewController.txtErrorMessage;
        txtError.setVisible(true);
        txtError.setText(errorMsg);
    }

    private void hideErrorMsg()
    {
        Label txtError = loginViewController.txtErrorMessage;
        txtError.setVisible(false);
        txtError.setText("");
    }
}
