package de.itlobby.services;

import de.itlobby.customcontrols.UserTab;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.framework.ViewManager;
import de.itlobby.models.Task;
import de.itlobby.models.UpdateInfo;
import de.itlobby.models.UpdateType;
import de.itlobby.models.User;
import de.itlobby.notification.TrayNotification;
import de.itlobby.settings.Settings;
import de.itlobby.util.ControlUtil;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractService
{
    public void onMarkAsDone(boolean isDone, TextFlow textField, HBox hBox, User user, Task task)
    {
        if (isDone)
        {
            ControlUtil.addCSSClass(hBox, "task_row_done");
            textField.setDisable(true);
        }
        else
        {
            ControlUtil.setOnlyCSSClass(hBox, "task_row");
            textField.setDisable(false);
        }
        task.setIsDone(isDone);
        BackendService.getInstance().updateUser(user);
    }

    public void onAddTask()
    {
        LightBoxService lightBoxService = ServiceLocator.getServiceInstance(LightBoxService.class);

        GridPane gridPane = new GridPane();
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        TextArea textArea = new TextArea();
        textArea.setWrapText(true);
        textArea.setMaxWidth(300);

        DatePicker datePicker = new DatePicker();

        gridPane.add(new Label("Aufgabenbeschreibung:"), 0, 0);
        gridPane.add(textArea, 1, 0);

        gridPane.add(new Label("Fälligkeitsdatum"), 0, 1);
        gridPane.add(datePicker, 1, 1);

        lightBoxService.showDialog("Aufgabe erstellen", gridPane, lightBoxService::hideDialog, () -> addTask(lightBoxService, textArea, datePicker));
    }

    public void onEditTask()
    {
        LightBoxService lightBoxService = ServiceLocator.getServiceInstance(LightBoxService.class);
        Task selectedTask = ControlUtil.getSelectedTaskFromTab((UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem());

        GridPane gridPane = new GridPane();
        gridPane.setVgap(5);
        gridPane.setHgap(5);

        TextArea textArea = new TextArea();
        textArea.setText(selectedTask.getText());
        textArea.setWrapText(true);
        textArea.setMaxWidth(300);

        DatePicker datePicker = new DatePicker();
        LocalDate localDate = selectedTask.getEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        datePicker.setValue(localDate);

        gridPane.add(new Label("Aufgabenbeschreibung:"), 0, 0);
        gridPane.add(textArea, 1, 0);

        gridPane.add(new Label("Fälligkeitsdatum"), 0, 1);
        gridPane.add(datePicker, 1, 1);

        lightBoxService.showDialog("Aufgabe bearbeiten", gridPane, lightBoxService::hideDialog, () -> editTask(lightBoxService, textArea, datePicker, selectedTask));
    }

    public void onRemoveTask()
    {
        LightBoxService lightBoxService = ServiceLocator.getServiceInstance(LightBoxService.class);
        Task selectedTask = ControlUtil.getSelectedTaskFromTab((UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem());


        lightBoxService.showDialog("Aufgabe löschen", new TextFlow(new Text("Möchten sie die Aufgabe wirklich löschen?")), lightBoxService::hideDialog, () -> deleteTask(lightBoxService, selectedTask));
    }

    private void deleteTask(LightBoxService lightBoxService, Task selectedTask)
    {
        UserTab selectedItem = (UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem();
        User tabUser = selectedItem.getTabUser();

        tabUser.getTaskList().remove(selectedTask);
        BackendService.getInstance().deleteTask(selectedTask);

        selectedItem.updateTaskListView(tabUser);

        lightBoxService.hideDialog();
    }

    private void editTask(LightBoxService lightBoxService, TextArea textArea, DatePicker datePicker, Task selectedTask)
    {
        UserTab selectedItem = (UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem();
        User tabUser = selectedItem.getTabUser();
        int indexOf = tabUser.getTaskList().indexOf(selectedTask);
        tabUser.getTaskList().remove(indexOf);

        selectedTask.setText(textArea.getText());
        selectedTask.setIsDone(false);

        LocalDate value = datePicker.getValue();
        if (value != null)
        {
            Instant instant = value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            selectedTask.setEndDate(Date.from(instant));

            Task updatedTask = BackendService.getInstance().updateTask(selectedTask);

            tabUser.getTaskList().add(indexOf, updatedTask);

            selectedItem.updateTaskListView(tabUser);

            lightBoxService.hideDialog();
        }
    }

    private void addTask(LightBoxService lightBoxService, TextArea textArea, DatePicker datePicker)
    {
        Task newTask = new Task();
        newTask.setText(textArea.getText());
        newTask.setIsDone(false);

        LocalDate value = datePicker.getValue();

        if (value != null)
        {
            Instant instant = value.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
            newTask.setEndDate(Date.from(instant));

            UserTab selectedItem = (UserTab) mainViewController.tabUserTasks.getSelectionModel().getSelectedItem();
            User tabUser = selectedItem.getTabUser();
            tabUser.getTaskList().add(newTask);

            tabUser = BackendService.getInstance().createTask(tabUser.getId(), newTask);

            selectedItem.updateTaskListView(tabUser);

            lightBoxService.hideDialog();
        }
    }

    public void executeUserUpdate(UpdateInfo updateInfo)
    {
        List<User> oldUsers = new ArrayList<>();

        for (Tab tab : mainViewController.tabUserTasks.getTabs())
        {
            UserTab userTab = (UserTab) tab;
            User tabUser = userTab.getTabUser();
            oldUsers.add(tabUser);
        }
        List<User> newUsers = BackendService.getInstance().getAllUser();

        String currentUserName = Settings.getInstance().getCurrentUser().getName();
        int oldTaskListSize = oldUsers.stream().filter(x -> x.getName().equals(currentUserName)).findFirst().get().getTaskList().size();
        int newTaskListSize = newUsers.stream().filter(x -> x.getName().equals(currentUserName)).findFirst().get().getTaskList().size();

        int diffTaskSize = Math.abs(oldTaskListSize - newTaskListSize);

        if (updateInfo.getUpdateType() != null)
        {
            UpdateType updateType = updateInfo.getUpdateType();

            switch (updateType)
            {
                case ADD:
                    TrayNotification.show(
                        "Neue Aufgabe",
                        "Sie haben " + diffTaskSize + " neue Aufgabe(n)",
                        new Image("icon/icon_32x32.png"),
                        ViewManager.getInstance().getPrimaryStage()
                    );
                    break;
                case EDIT:
                    TrayNotification.show(
                        "Geänderte Aufgabe",
                        "Ihre Aufgaben wurden aktualisiert",
                        new Image("icon/icon_32x32.png"),
                        ViewManager.getInstance().getPrimaryStage()
                    );
                    break;
                case REMOVE:
                    TrayNotification.show(
                        "Gelöschte Aufgaben",
                        "Es wurden " + diffTaskSize + " Aufgabe(n) gelöscht",
                        new Image("icon/icon_32x32.png"),
                        ViewManager.getInstance().getPrimaryStage()
                    );
                    break;
            }
        }

        MainService mainService = ServiceLocator.getServiceInstance(MainService.class);
        mainService.refreshTabView();

        BackendService.getInstance().registerMachine();
    }

    private List<Task> getTasksByUser(List<User> oldUsers, User newUser)
    {
        for (User oldUser : oldUsers)
        {
            if (oldUser.equals(newUser))
            {
                return oldUser.getTaskList();
            }
        }

        return new ArrayList<>();
    }
}
