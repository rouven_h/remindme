package de.itlobby.util;

import de.itlobby.customcontrols.TaskLine;
import de.itlobby.customcontrols.UserTab;
import de.itlobby.models.Task;
import de.itlobby.models.User;
import javafx.application.Platform;
import javafx.css.Styleable;
import javafx.scene.Node;
import javafx.scene.control.ListView;


public class ControlUtil
{
    public static ListView createUserTaskListView(User user)
    {
        ListView listView = new ListView();
        listView.setStyle("-fx-background-color: transparent");

        for (int i = 0; i < user.getTaskList().size(); i++)
        {
            Task task = user.getTaskList().get(i);
            TaskLine listViewRow = new TaskLine(task, user);
            listView.getItems().add(listViewRow);
        }

        return listView;
    }

    public static void setOnlyCSSClass(Styleable node, String styleClass)
    {
        node.getStyleClass().clear();
        node.getStyleClass().add(styleClass);
    }

    public static void addCSSClass(Styleable node, String... styleClasses)
    {
        for (String styleClass : styleClasses)
        {
            node.getStyleClass().add(styleClass);
        }
    }

    public static Task getSelectedTaskFromTab(UserTab userTab)
    {
        ListView listView = userTab.getCurrentTaskListView();

        TaskLine selectedItem = (TaskLine) listView.getSelectionModel().getSelectedItem();

        return selectedItem.getTask();
    }

    public static void focusNode(Node node)
    {
        Platform.runLater(node::requestFocus);
    }
}
