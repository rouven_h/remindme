package de.itlobby.util;

import java.net.URL;

public class SystemUtil
{
    public static URL getResourceURL(String path)
    {
        return Thread.currentThread().getContextClassLoader().getResource(path);
    }

    public static void Sleep(int timeInMs)
    {
        try
        {
            Thread.sleep(timeInMs);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
