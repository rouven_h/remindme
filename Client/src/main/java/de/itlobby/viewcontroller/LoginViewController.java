package de.itlobby.viewcontroller;

import de.itlobby.models.ViewController;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class LoginViewController implements ViewController
{
    public TextField txtUsername;
    public Button btnExit;
    public Button btnLogin;
    public Label txtErrorMessage;
    public ImageView imgBackendStatus;
}
