package de.itlobby.viewcontroller;

import de.itlobby.models.ViewController;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;

public class MainViewController implements ViewController
{
    public MenuItem btnCloseApplication;
    public MenuItem btnLogout;
    public MenuItem btnAbout;
    public TabPane tabUserTasks;
    public BorderPane lbBorderPane;
    public MenuItem btnDeleteUser;
}
