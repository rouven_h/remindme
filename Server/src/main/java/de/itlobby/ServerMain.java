package de.itlobby;

import de.itlobby.dto.TaskDTO;
import de.itlobby.dto.UserDTO;
import de.itlobby.entities.Task;
import de.itlobby.entities.User;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.services.PersistenceService;
import de.itlobby.settings.Settings;
import de.itlobby.ws.MachineResource;
import de.itlobby.ws.TaskResource;
import de.itlobby.ws.UserResource;
import org.apache.log4j.BasicConfigurator;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.simple.SimpleContainerFactory;
import org.glassfish.jersey.simple.SimpleServer;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServerMain
{
    public static void main(String[] args)
    {
        try
        {
            BasicConfigurator.configure();

            initializeDatabase();
            initializeWebServer();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void createfakes()
    {
        for (int i = 0; i < 5; i++)
        {
            UserDTO user = new UserDTO();
            user.setName("User " + i);

            List<TaskDTO> taskList = new ArrayList<>();

            for (int j = 0; j < 5; j++)
            {
                TaskDTO task = new TaskDTO();
                task.setEndDate(new Date());
                task.setText(user.getName() + " bitte mache das hier: Aufgabe" + j + ", schnell!");
                task.setIsDone(true);

                taskList.add(task);
            }

            user.setTaskList(taskList);

            PersistenceService service = ServiceLocator.getServiceInstance(PersistenceService.class);

            service.fakeUser(user);
        }
    }

    private static void initializeDatabase()
    {
        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);

        Configuration configuration = new Configuration();

        configuration.configure().addPackage("de/itlobby/entities").addAnnotatedClass(User.class);
        configuration.configure().addPackage("de/itlobby/entities").addAnnotatedClass(Task.class);

        configuration.configure().setProperty("hibernate.archive.autodetection", "class");
        configuration.configure().setProperty("hibernate.show_sql", "false");
        configuration.configure().setProperty("hibernate.format_sql", "false");
        configuration.configure().setProperty("hibernate.hbm2ddl.auto", "update");

        StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
            .applySettings(configuration.getProperties())
            .build();

        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);

        persistenceService.setSessionFacotry(factory);
    }

    private static void initializeWebServer()
    {
        String serverName = Settings.getInstance().getConfig().getServerName();
        Integer port = Integer.valueOf(Settings.getInstance().getConfig().getServerPort());

        URI baseUri = UriBuilder.fromUri("http://" + serverName + "/").port(port).build();

        ResourceConfig config = new ResourceConfig(
            UserResource.class,
            MachineResource.class,
            TaskResource.class
        );

        SimpleServer server = SimpleContainerFactory.create(baseUri, config);
    }
}
