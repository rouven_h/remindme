package de.itlobby.database;

import org.hsqldb.util.DatabaseManagerSwing;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * => Created by rhi on 08.08.2014.
 */
public class HsqlTest
{
    public static void main(String[] args) throws SQLException
    {
        Connection conn = null;

        try
        {
            //Class.forName("org.hsqldb.jdbcDriver");

            DatabaseManagerSwing.main(args);

//            conn = DriverManager.getConnection("jdbc:hsqldb:"
//                                                   + "remindme",
//                                               "sa",
//                                               ""
//            );
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (conn != null)
                {
                    conn.close();
                }
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }
}
