package de.itlobby.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@XmlRootElement
public class TaskDTO implements Serializable
{
    private Long id;
    private String text;
    private Boolean isDone;
    private Date endDate;

    public TaskDTO()
    {
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Boolean getIsDone()
    {
        return isDone;
    }

    public void setIsDone(Boolean isDone)
    {
        this.isDone = isDone;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}

