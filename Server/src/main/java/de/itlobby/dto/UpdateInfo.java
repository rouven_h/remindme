package de.itlobby.dto;

import java.io.Serializable;

public class UpdateInfo implements Serializable
{
    private boolean needUpdate;
    private UpdateType updateType;

    public UpdateInfo()
    {
    }

    public UpdateInfo(UpdateType updateType)
    {
        this.updateType = updateType;
        this.needUpdate = true;
    }

    public UpdateInfo(boolean needUpdate)
    {
        this.needUpdate = needUpdate;
        this.updateType = null;
    }

    public boolean isNeedUpdate()
    {
        return needUpdate;
    }

    public void setNeedUpdate(boolean needUpdate)
    {
        this.needUpdate = needUpdate;
    }

    public UpdateType getUpdateType()
    {
        return updateType;
    }

    public void setUpdateType(UpdateType updateType)
    {
        this.updateType = updateType;
    }
}
