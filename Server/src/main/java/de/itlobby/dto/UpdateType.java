package de.itlobby.dto;

import java.io.Serializable;

public enum UpdateType implements Serializable
{
    MSIC,
    ADD,
    EDIT,
    REMOVE
}
