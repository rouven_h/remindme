package de.itlobby.dto;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UserDTO
{
    private Long id;
    private String name;
    private List<TaskDTO> taskList;

    public UserDTO()
    {
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<TaskDTO> getTaskList()
    {
        return taskList;
    }

    public void setTaskList(List<TaskDTO> taskList)
    {
        this.taskList = taskList;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }
}
