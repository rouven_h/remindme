package de.itlobby.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "task")
public class Task
{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text")
    private String text;

    @Column(name = "is_done")
    private Boolean isDone;

    @Column(name = "end_date")
    private Date endDate;

    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private User user;

    public Task()
    {
    }

    public Task(long id, String text, boolean isDone, Date endDate)
    {
        this.id = id;
        this.text = text;
        this.isDone = isDone;
        this.endDate = endDate;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Boolean getIsDone()
    {
        return isDone;
    }

    public void setIsDone(Boolean isDone)
    {
        this.isDone = isDone;
    }

    public Date getEndDate()
    {
        return endDate;
    }

    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Task task = (Task) o;

        if (endDate != null ? !(endDate.getTime() == task.endDate.getTime()) : task.endDate != null)
        {
            return false;
        }
        if (isDone != null ? !isDone.equals(task.isDone) : task.isDone != null)
        {
            return false;
        }
        if (text != null ? !text.equals(task.text) : task.text != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (isDone != null ? isDone.hashCode() : 0);
        result = 31 * result + (endDate != null ? ((Long) endDate.getTime()).hashCode() : 0);
        return result;
    }
}

