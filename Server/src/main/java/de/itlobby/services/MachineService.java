package de.itlobby.services;

import de.itlobby.dto.UpdateInfo;
import de.itlobby.dto.UpdateType;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class MachineService
{
    private Logger LOG = Logger.getLogger(MachineService.class);

    private Map<String, UpdateInfo> activeMachines = new HashMap<>();

    public String registerMachine(String machineName)
    {
        LOG.info("registerMachine " + machineName);

        activeMachines.put(machineName, new UpdateInfo(false));

        return machineName;
    }

    public boolean unregisterMachine(String machineName)
    {
        LOG.info("unregisterMachine " + machineName);

        activeMachines.remove(machineName);

        return true;
    }

    public void setAllNeedUpdate(UpdateType updateType)
    {
        LOG.info("setAllNeedUpdate " + updateType.name());

        for (String machineName : activeMachines.keySet())
        {
            activeMachines.put(machineName, new UpdateInfo(updateType));
        }
    }

    public UpdateInfo checkForUpdate(String machinename)
    {
        LOG.info("checkForUpdate " + machinename);

        return activeMachines.get(machinename);
    }
}
