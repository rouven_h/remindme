package de.itlobby.services;

import de.itlobby.dto.TaskDTO;
import de.itlobby.dto.UpdateType;
import de.itlobby.dto.UserDTO;
import de.itlobby.entities.Task;
import de.itlobby.entities.User;
import de.itlobby.framework.DTOUtil;
import de.itlobby.framework.ServiceLocator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

public class PersistenceService
{
    private SessionFactory sessionFacotry;

    public void setSessionFacotry(SessionFactory sessionFacotry)
    {
        this.sessionFacotry = sessionFacotry;
    }

    public void fakeUser(UserDTO user)
    {
        User newUser = new User();
        newUser.setName(user.getName());
        newUser.setTaskList(new ArrayList<>());

        for (TaskDTO taskDTO : user.getTaskList())
        {
            Task task = new Task();
            task.setText(taskDTO.getText());
            task.setEndDate(taskDTO.getEndDate());
            task.setIsDone(taskDTO.getIsDone());
            task.setUser(newUser);

            newUser.getTaskList().add(task);
        }

        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        session.saveOrUpdate(newUser);

        session.getTransaction().commit();
        session.close();
    }

    public List<User> getAllUser()
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        Query query = session.createQuery("from User");
        List<User> allUser = query.list();

        session.getTransaction().commit();
        session.close();

        return allUser;
    }

    public User getUserByName(String username)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        List<User> userList = session.createQuery("from User where name = ?")
            .setParameter(0, username).list();

        if (userList.size() <= 0)
        {
            userList.add(createNewUser(username));
        }

        session.getTransaction().commit();
        session.close();

        return userList.get(0);
    }

    private User createNewUser(String username)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        User user = new User();
        user.setName(username);
        User merge = (User) session.merge(user);

        session.getTransaction().commit();
        session.close();

        MachineService machineService = ServiceLocator.getServiceInstance(MachineService.class);
        machineService.setAllNeedUpdate(UpdateType.MSIC);

        return merge;
    }

    public User updateUser(UserDTO userDTO)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        List<User> list = session.createQuery("from User where name = ?").setParameter(0, userDTO.getName()).list();

        User merge;

        if (list.size() > 0)
        {
            User user = list.get(0);
            List<Task> taskList = DTOUtil.buildAll(userDTO.getTaskList(), Task.class);

            for (Task task : taskList)
            {
                task.setUser(user);
            }

            user.getTaskList().clear();
            user.getTaskList().addAll(taskList);

            merge = (User) session.merge(user);
        }
        else
        {
            throw new RuntimeException("Keinen User zu '" + userDTO.getName() + "' gefunden.");
        }

        session.getTransaction().commit();
        session.close();

        return merge;
    }

    public Boolean deleteUser(String username)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        List<User> userList = session.createQuery("from User where name = ?")
            .setParameter(0, username).list();

        boolean delOk = false;
        if (userList.size() > 0)
        {
            session.delete(userList.get(0));
            delOk = true;
        }

        session.getTransaction().commit();
        session.close();

        return delOk;
    }

    public boolean deleteTask(Long id)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        Task foundTask = (Task) session.get(Task.class, id);
        if (foundTask == null)
        {
            return false;
        }
        User user = foundTask.getUser();
        user.getTaskList().remove(foundTask);
        session.merge(user);

        session.getTransaction().commit();
        session.close();

        return true;
    }

    public Task updateTask(Task updated)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        Task foundTask = (Task) session.get(Task.class, updated.getId());
        updated.setUser(foundTask.getUser());
        Task merge = (Task) session.merge(updated);

        session.getTransaction().commit();
        session.close();

        return merge;
    }

    public User createTask(Long userId, Task taskToSave)
    {
        Session session = sessionFacotry.openSession();
        session.beginTransaction();

        User user = (User) session.get(User.class, userId);
        user.getTaskList().add(taskToSave);
        taskToSave.setUser(user);

        User mergedUser = (User) session.merge(user);

        session.getTransaction().commit();
        session.close();

        return mergedUser;
    }
}
