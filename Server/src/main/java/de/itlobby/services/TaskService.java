package de.itlobby.services;

import de.itlobby.dto.TaskDTO;
import de.itlobby.dto.UpdateType;
import de.itlobby.dto.UserDTO;
import de.itlobby.entities.Task;
import de.itlobby.entities.User;
import de.itlobby.framework.DTOUtil;
import de.itlobby.framework.ServiceLocator;
import org.apache.log4j.Logger;

public class TaskService
{
    private Logger LOG = Logger.getLogger(TaskService.class);

    public Boolean deleteTask(Long id)
    {
        LOG.info("deleteTask " + id);

        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);

        ServiceLocator.getServiceInstance(MachineService.class).setAllNeedUpdate(UpdateType.REMOVE);
        return persistenceService.deleteTask(id);
    }

    public TaskDTO updateTask(TaskDTO taskDTO)
    {
        LOG.info("updateTask " + taskDTO.getId());

        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);
        Task savedTask = persistenceService.updateTask(DTOUtil.build(taskDTO, Task.class));

        ServiceLocator.getServiceInstance(MachineService.class).setAllNeedUpdate(UpdateType.EDIT);
        return DTOUtil.build(savedTask, TaskDTO.class);
    }

    public UserDTO createTask(Long userId, TaskDTO taskDTO)
    {
        LOG.info("createTask " + taskDTO.getText());

        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);
        User savedUser = persistenceService.createTask(userId, DTOUtil.build(taskDTO, Task.class));

        ServiceLocator.getServiceInstance(MachineService.class).setAllNeedUpdate(UpdateType.ADD);
        return DTOUtil.build(savedUser, UserDTO.class);
    }
}
