package de.itlobby.services;

import de.itlobby.dto.TaskDTO;
import de.itlobby.dto.UpdateType;
import de.itlobby.dto.UserDTO;
import de.itlobby.entities.User;
import de.itlobby.framework.DTOUtil;
import de.itlobby.framework.ServiceLocator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class UserService
{
    private Logger LOG = Logger.getLogger(UserService.class);

    public UserDTO[] getAllUser()
    {
        LOG.info("getAllUser");

        PersistenceService service = ServiceLocator.getServiceInstance(PersistenceService.class);

        List<User> userList = service.getAllUser();

        List<UserDTO> retList = new ArrayList<>();

        for (User user : userList)
        {
            UserDTO userDTO = DTOUtil.build(user, UserDTO.class);
            userDTO.setTaskList(DTOUtil.buildAll(user.getTaskList(), TaskDTO.class));

            retList.add(userDTO);
        }

        UserDTO[] array = new UserDTO[retList.size()];
        retList.toArray(array);
        return array;
    }

    public UserDTO getUser(String username)
    {
        LOG.info("getUser " + username);

        PersistenceService service = ServiceLocator.getServiceInstance(PersistenceService.class);

        User user = service.getUserByName(username);

        return DTOUtil.build(user, UserDTO.class);
    }

    public UserDTO updateUser(UserDTO userDTO)
    {
        LOG.info("updateUser " + userDTO.getName());

        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);
        //User otherUser = persistenceService.getUserByName(userDTO.getName());
        //User mergedUser = MergeUtil.mergeUser(DTOUtil.build(userDTO, User.class), otherUser);
        //User savedUser = persistenceService.updateUser(DTOUtil.build(mergedUser, UserDTO.class));

        User savedUser = persistenceService.updateUser(userDTO);

        MachineService machineService = ServiceLocator.getServiceInstance(MachineService.class);
        machineService.setAllNeedUpdate(UpdateType.EDIT);

        return DTOUtil.build(savedUser, UserDTO.class);
    }

    public Boolean deleteUser(String username)
    {
        LOG.info("deleteUser " + username);

        PersistenceService persistenceService = ServiceLocator.getServiceInstance(PersistenceService.class);
        return persistenceService.deleteUser(username);
    }
}
