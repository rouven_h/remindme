package de.itlobby.utils;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MapUtil
{
    public static String toJSON(Object object)
    {
        OutputStream outputStream = null;

        try
        {
            ObjectMapper mapper = new ObjectMapper();

            outputStream = new ByteArrayOutputStream();

            mapper.writeValue(outputStream, object);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return outputStream.toString();
    }

    public static <T> T toObject(String jsonString, Class<T> targetClass)
    {
        try
        {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(jsonString, targetClass);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
