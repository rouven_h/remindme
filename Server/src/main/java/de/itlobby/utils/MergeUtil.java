package de.itlobby.utils;

import de.itlobby.entities.Task;
import de.itlobby.entities.User;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class MergeUtil
{
    public static User mergeUser(User newUser, User otherUser)
    {
        List<Task> newTasks = newUser.getTaskList();
        List<Task> otherTasks = otherUser.getTaskList();
        HashSet<Task> mergedTasks = new HashSet<>();
        User merged = new User();
        merged.setName(otherUser.getName());
        merged.setId(otherUser.getId());

        List<Task> equalTasks = getEqual(newTasks, otherTasks);
        mergedTasks.addAll(equalTasks);

        List<Task> diff1 = getDiffs(newTasks, otherTasks);
        List<Task> diff2 = getDiffs(otherTasks, newTasks);

        mergedTasks.addAll(diff1);
        mergedTasks.addAll(diff2);

        merged.setTaskList(new ArrayList<>(mergedTasks));
        return merged;
    }

    private static List<Task> getDiffs(List<Task> newTasks, List<Task> otherTasks)
    {
        List<Task> diff = new ArrayList<>();

        for (Task newTask : newTasks)
        {
            boolean foundOne = false;

            for (Task otherTask : otherTasks)
            {
                if (newTask.equals(otherTask))
                {
                    foundOne = true;
                    break;
                }
            }

            if (!foundOne)
            {
                diff.add(newTask);
            }
        }

        return diff;
    }

    private static List<Task> getEqual(List<Task> newTasks, List<Task> otherTasks)
    {
        List<Task> equal = new ArrayList<>();

        for (Task newTask : newTasks)
        {
            for (Task otherTask : otherTasks)
            {
                if (newTask.equals(otherTask))
                {
                    equal.add(newTask);
                    break;
                }
            }
        }

        return equal;
    }
}
