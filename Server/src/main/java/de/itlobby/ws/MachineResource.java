package de.itlobby.ws;

import de.itlobby.framework.ServiceLocator;
import de.itlobby.services.MachineService;
import de.itlobby.utils.MapUtil;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("machine")
public class MachineResource
{
    private final MachineService machineService;

    public MachineResource()
    {
        machineService = ServiceLocator.getServiceInstance(MachineService.class);
    }

    @POST
    @Path("registerMachine")
    @Produces(MediaType.APPLICATION_JSON)
    public String registerMachine(String machinename)
    {
        return machineService.registerMachine(machinename);
    }

    @GET
    @Path("{machinename}")
    @Produces(MediaType.APPLICATION_JSON)
    public String checkForUpdate(@PathParam("machinename") String machinename)
    {
        return MapUtil.toJSON(machineService.checkForUpdate(machinename));
    }

    @DELETE
    @Path("{machinename}")
    @Produces(MediaType.APPLICATION_JSON)
    public String unregisterMachine(@PathParam("machinename") String machinename)
    {
        return MapUtil.toJSON(machineService.unregisterMachine(machinename));
    }
}
