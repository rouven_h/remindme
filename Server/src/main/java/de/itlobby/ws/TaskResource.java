package de.itlobby.ws;

import de.itlobby.dto.TaskDTO;
import de.itlobby.dto.UserDTO;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.services.TaskService;
import de.itlobby.utils.MapUtil;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("task")
public class TaskResource
{
    private final TaskService taskService;

    public TaskResource()
    {
        taskService = ServiceLocator.getServiceInstance(TaskService.class);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteUser(@PathParam("id") String id)
    {
        return MapUtil.toJSON(taskService.deleteTask(MapUtil.toObject(id, Long.class)));
    }

    @POST
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    public String updateTask(String task)
    {
        TaskDTO taskDTO = MapUtil.toObject(task, TaskDTO.class);
        TaskDTO updatedTask = taskService.updateTask(taskDTO);
        return MapUtil.toJSON(updatedTask);
    }

    @POST
    @Path("create")
    @Consumes(MediaType.APPLICATION_JSON)
    public String createTask(String stringArray)
    {
        String[] params = MapUtil.toObject(stringArray, String[].class);

        Long userId = MapUtil.toObject(params[0], Long.class);
        TaskDTO taskDTO = MapUtil.toObject(params[1], TaskDTO.class);

        UserDTO updatedUser = taskService.createTask(userId, taskDTO);

        return MapUtil.toJSON(updatedUser);
    }
}
