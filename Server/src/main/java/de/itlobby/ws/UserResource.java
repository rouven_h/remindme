package de.itlobby.ws;

import de.itlobby.dto.UserDTO;
import de.itlobby.framework.ServiceLocator;
import de.itlobby.services.UserService;
import de.itlobby.utils.MapUtil;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("user")
public class UserResource
{
    private final UserService userService;

    public UserResource()
    {
        userService = ServiceLocator.getServiceInstance(UserService.class);
    }

    @GET
    @Path("ping")
    @Produces(MediaType.APPLICATION_JSON)
    public String ping()
    {
        return MapUtil.toJSON(System.currentTimeMillis());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllUser()
    {
        return MapUtil.toJSON(userService.getAllUser());
    }

    @GET
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUser(@PathParam("username") String username)
    {
        return MapUtil.toJSON(userService.getUser(username));
    }

    @DELETE
    @Path("{username}")
    @Produces(MediaType.APPLICATION_JSON)
    public String deleteUser(@PathParam("username") String username)
    {
        return MapUtil.toJSON(userService.deleteUser(username));
    }

    @POST
    @Path("update")
    @Consumes(MediaType.APPLICATION_JSON)
    public String updateUser(String user)
    {
        UserDTO userDTO = MapUtil.toObject(user, UserDTO.class);
        UserDTO updateUser = userService.updateUser(userDTO);
        return MapUtil.toJSON(updateUser);
    }
}
